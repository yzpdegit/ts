import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class tool {
    public static boolean compare(String[] array,String word)//统计字符传中是否存在该单词
    {
        for(int i=0;i<array.length;i++)
        {
            if(array[i].equals(word))
                return true;
        }
        return false;
    }
    public static int getPosition(String[] array,String word)//该单词位置
    {
        for(int i=0;i<array.length;i++)
        {
            if(array[i].equals(word))
                return i;
        }
        return -1;
    }
    public static void setCount(String[] array,String path) throws IOException//执行命令
    {
        function f=new function();
        if(compare(array,"-s"))
        {
            String path1=path;
            path=path.replaceAll("[.]","[.]");//转义字符转义，将有特殊意义的字符转化为普通字符
            path=path.replaceAll("[*]",".*");
            getPath(new File("."),array,f,path,getPosition(array,path1));
        }
        else
        {
            ready(array,path,f,getPosition(array,path));
           read(array,f,path);
        }
    }
    public static void getPath(File file1,String[] array,function fun,String path,int count) throws IOException//是否解析子目录
    {
        File[] file=file1.listFiles();
        for(File f: file)
        {
            if(f.isDirectory())
            {
                getPath(f,array,fun,path,count);
            }
            else
            {
                if(Pattern.matches(path,f.getName()))
                {
                    ready(array, f.getAbsolutePath(), fun,count);
                    read(array, fun,f.getName());
                }
            }
        }
    }
    public static void ready(String[] array,String path,function f,int count) throws IOException {
        count--;
        f.init();
        if(compare(array,"-e"))
        {
            String path1=array[getPosition(array,"-e")+1];
            InputStreamReader input1=new InputStreamReader(new FileInputStream(path1));
             BufferedReader br1=new BufferedReader(input1);
            List<String> list1=new ArrayList<>();
            while (br1.read() != -1) {
                String s = br1.readLine();
                if(s!=null)
                {
                    String[] s1 = s.split(",| ");
                    for (int j = 0; j < s1.length; j++)
                    {
                        if (!s1[j].equals(""))
                        {
                            list1.add(s1[j]);
                        }
                    }
                }
            }
            input1.close();
            for(;count>0;count--)
            {
                f.readCommand(array[count],path,list1);
            }
        }else
        {
            for(;count>0;count--)
            {
                f.readCommand(array[count],path,null);
            }
        }
    }
    public static  void read(String[] array,function f,String path) throws IOException {
        File result;
        if(compare(array,"-o"))
        {
            result=new File(array[getPosition(array,"-o")+1]);
        }
        else
        {
            result=new File("result.txt");
        }
        if(result.exists()!=true)
        {
            result.createNewFile();
        }
        BufferedWriter bd=new BufferedWriter(new FileWriter(result,true));
        if(f.getWord()!=0)
            bd.write(path+",单词数:"+f.getWord());bd.newLine();
        if(f.getCharacter()!=0)
            bd.write(path+",字符数:"+f.getCharacter());bd.newLine();
        if(f.getLength()!=0)
            bd.write(path+",行数:"+f.getLength());bd.newLine();
        if(f.getCode()!=null)
            bd.write(path+",代码行/空行/注释行:"+f.getCode());bd.newLine();
        bd.flush();
        bd.close();
    }
}
