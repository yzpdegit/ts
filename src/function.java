import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class function {
    private int word;//单词数
    private int  character;//字符数
    private int  length;//行数
    private String code;//空行数
    public void init()
    {
        word=0;character = 0;length=0;code=null;
    }
    public String commandW(InputStreamReader input,BufferedReader br,List<String> list1) throws IOException {
        List<String> list=new ArrayList<>();
        while(br.read()!=-1)
        {
            String s=br.readLine();
            if(s!=null) {
                String[] s1 = s.split(",| ");
                for (int i = 0; i < s1.length; i++) {
                    if (!s1[i].equals("")) {
                        list.add(s1[i]);
                        word++;
                    }
                }
            }
        }
        if(list1!=null) {
            String[] str=(String[])list.toArray(new String[list.size()]);
            String[] str1=(String[])list1.toArray(new String[list1.size()]);
            for(int i=0;i<str.length;i++)
            {
                for(int j=0;j<str1.length;j++)
                {
                    if(str[i].equalsIgnoreCase(str1[j]))
                    {
                        word--;
                        break;
                    }
                }
            }
        }
       return "单词数"+word;
    }
    public void commandA(InputStreamReader input,BufferedReader br) throws IOException {
        int code1=0,zhushi=0,empty=0;
        while(br.read()!=-1)
        {
            int k=0,m=0;
            String string=br.readLine();
            if(string!=null) {
                String[] s1 = string.split("//");
                char s[] = s1[0].toCharArray();
                for (int i = 0; i < s.length; i++)
                {
                    if (s[i] > 32 && s[i] < 128)
                    {
                        k++;//读取到代码行的一个合法字符
                        m = i;//保存改合法字符位置
                    }
                }
                if (s.length == 0)
                {
                    if (s1.length == 1)
                        empty++;
                    else
                        zhushi++;
                }
                else
                    {
                    if (k > 1)
                        code1++;
                    if (k == 1 && s[m] > 32 && s[m] < 127)
                    {
                        if (s1.length == 1)
                            empty++;
                        else
                            zhushi++;
                    }
                    if (k == 0) empty++;
                }
            }
        }
        code=code1+"/"+empty+"/"+zhushi;
    }
    public String readCommand(String command,String path,List<String> list1) throws IOException {
        InputStreamReader input=new InputStreamReader(new FileInputStream(path));
        BufferedReader br=new BufferedReader(input);
        switch (command)
        {
            case "-c":
                while(br.read()!=-1)
                {
                    String s=br.readLine();
                    if(s!=null)
                    character+=s.length()+s.split(" ").length-1;
                }
                return "字符数"+character;

            case "-w":
               return commandW(input,br,list1);

            case "-l":
                while(br.read()!=-1)
                {
                    String s=br.readLine();
                    length++;
                }
                return "行数"+length;

            case  "-a":
                commandA(input,br);
                return null;
        }
        input.close();
        return null;
    }

    public int getWord() {
        return word;
    }

    public void setWord(int word) {
        this.word = word;
    }

    public int getCharacter() {
        return character;
    }

    public void setCharacter(int character) {
        this.character = character;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
