import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class UI implements ActionListener {

    /*
     * ＵＩ相关
     * */
    private  JButton buttonF = null;
    private JButton buttonOK = null;
    private JFrame frame = null;
    private JTextArea jTextArea = null;
    private Container conFile = null;
    private Container conResult = null;
    private  JLabel jLabel = null;
    private JTextField textF = null;
    private JFileChooser jFileChooser = null;
    private JTabbedPane tabbedPane = null;

    /*
     * WordCound　调用参数相关
     * */
    private File file = null;
    //private Parameter parameter = null;

    public UI(){
        // this.parameter = parameter;
        buttonF = new JButton();
        buttonOK = new JButton();
        frame = new JFrame("WordCount");
        jTextArea = new JTextArea();
        conFile=new Container();
        conResult=new Container();
        jLabel = new JLabel();
        textF=new JTextField();
        tabbedPane = new JTabbedPane();
        jFileChooser = new JFileChooser();

    }

    public void createAndShowGUI() {
        // 确保一个漂亮的外观风格
        //JFrame.setDefaultLookAndFeelDecorated(true);

        // 创建及设置窗口
        //frame.getContentPane().setBackground(Color.red);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        /*
         *获取屏幕的宽，高
         *
         * */
        double hostL = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double hostW = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        frame.setLocation((int)(hostL/2-100),(int)(hostW/2-250));//根据面板大小设置面板出现位置
        frame.setSize(500,200);//设置整个面板大小
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);

        /*
         *添加面板
         *
         * */
        tabbedPane.addTab("chose your file",conFile);
        tabbedPane.addTab("results",conResult);

        /*
         *设置结果显示文本框
         *
         * */
        jTextArea.setBounds(0,0,500,200);
        jTextArea.setLineWrap(true);
        //jTextArea.setText("464648564444444444444416555555555555555555555555555555555555555555");
        //add to pane
        conResult.add(jTextArea);

        /*
         * 设置文件选择框
         * */
        jLabel.setText("file: ");
        jLabel.setSize(40,40);
        jLabel.setLocation(30,40);
        conFile.add(jLabel);

        //文件路径名
        textF.setSize(180,25);
        textF.setLocation(70,50);
        conFile.add(textF);

        //文件选择框
        jFileChooser.setCurrentDirectory(new File("").getAbsoluteFile());
        conFile.add(jFileChooser);

        /*
         *设置文件选择按钮和确认按钮
         *
         * */
        //文字
        buttonF.setText("chose");
        buttonOK.setText("sure");
        //位置
        buttonF.setLocation(270,50);
        buttonOK.setLocation(340,50);
        //大小
        buttonF.setSize(50,25);
        buttonOK.setSize(50,25);
        //内边距
        buttonF.setMargin(new Insets(0,0,0,0));
        buttonOK.setMargin(new Insets(0,0,0,0));
        //加入pane
        conFile.add(buttonF);
        conFile.add(buttonOK);
        //添加监听
        buttonF.addActionListener(this);
        buttonOK.addActionListener(this);

        frame.add(tabbedPane);
        // 显示窗口
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {

        if(actionEvent.getSource().equals(buttonF)) {
            jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int status = jFileChooser.showOpenDialog(null);
            if(status==JFileChooser.CANCEL_OPTION){
                return;
            }else if(JFileChooser.APPROVE_OPTION == status){
                file=jFileChooser.getSelectedFile();//选择到的文件
                System.out.println(file.exists());
                textF.setText(file.getAbsolutePath());
            }

        }else if(actionEvent.getSource().equals(buttonOK)){
            if(file==null){
                // 消息对话框无返回, 仅做通知作用
                JOptionPane.showMessageDialog( frame, "File is not exist !!!", "error", JOptionPane.INFORMATION_MESSAGE );
            }else{

                tabbedPane.setSelectedComponent(conResult);
                String filePath = file.getAbsolutePath();
                String sResult= "";
                File file1=new File("result.txt");
                if(file1.exists())
                {
                    file1.delete();
                }
                function f=new function();
                try {
                    sResult=sResult+" "+f.readCommand("-c",filePath,null);
                    sResult=sResult+" "+f.readCommand("-w",filePath,null);
                    sResult=sResult+" "+f.readCommand("-l",filePath,null);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                jTextArea.setText(sResult);
            }
        }
    }



}
