import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args)throws IOException {
        Scanner in=new Scanner(System.in);
        System.out.println("输入命令:");
        String command=in.nextLine();
        String[] array=command.split(" ");
        File file;
        if(array.length>2)
        {
            if(tool.compare(array,"-e")&&tool.compare(array,"-o"))//都存在
            {
                file=new File(array[tool.getPosition(array,"-o")+1]);
                if(file.exists())
                {
                    file.delete();
                }
                tool.setCount(array,array[array.length-5]);
            }else if(tool.compare(array,"-e")||tool.compare(array,"-o"))//只存在一个
            {
                if(tool.compare(array,"-o"))
                {
                    file=new File(array[tool.getPosition(array,"-o")+1]);
                    if(file.exists())
                    {
                        file.delete();
                    }
                }
                else
                {
                    file=new File("result.txt");
                    if(file.exists())
                    {
                        file.delete();
                    }
                }
                tool.setCount(array,array[array.length-3]);

            }else//都不存在
            {
                file=new File("result.txt");
                if(file.exists())
                {
                    file.delete();
                }
                tool.setCount(array,array[array.length-1]);
            }
        }else
        {
            UI u=new UI();
            u.createAndShowGUI();
        }

    }

}
